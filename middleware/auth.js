const jwt = require('jsonwebtoken');
const config = require('config');

module.exports = function auth(req, res, next) {
  const token = req.header('x-auth-token');

  if (!token) {
    return res.status(401).json({ msg: 'Отказано в доступе. Нет токена авторизации' });
  }

  try {
    const decoded = jwt.verify(token, config.get('JWT_TOKEN'));

    req.user = decoded.user;
    next();
  } catch (err) {
    res.status(401).json({ msg: 'Невалидный токен авторизации' });
  }
};
