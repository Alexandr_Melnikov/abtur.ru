import mongoose from 'mongoose';

const ContactsSchema = new mongoose.Schema({
  mainPhone: {
    type: String,
  },
  additionalPhone: {
    type: String,
  },
  address: {
    type: String,
  },
}, { versionKey: false });

export default mongoose.model('Contacts', ContactsSchema);
