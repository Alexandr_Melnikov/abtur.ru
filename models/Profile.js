import mongoose from 'mongoose';

const UserSchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user',
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  date: {
    type: Date,
    default: Date.now(),
  },
}, { versionKey: false });

// module.exports = User = mongoose.model('user', UserSchema);
export default mongoose.model('User', UserSchema);
