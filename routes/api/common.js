import express from 'express';
import axios from 'axios';
import fastParser from 'fast-xml-parser';

const router = express.Router();

// @route   api/news/
// @desc    Get news
router.get(
  '/news',
  async (req, res) => {
    try {
      const { data } = await axios.get('https://tourism.gov.ru/news/rss/');
      const parsedNews = fastParser.parse(data);
      const news = parsedNews.rss.channel.item.sort((a, b) => new Date(b.pubDate) - new Date(a.pubDate))
        .slice(0, 10);
      res.json(news);
    } catch (err) {
      res.status(500)
        .json({
          message: 'Не удалось загрузить новости, попробуйте перезагрузить страницу',
          messageType: 'error',
        });
    }
  },
);

// @route   api/currency/
// @desc    Get currency
router.get(
  '/currency',
  async (req, res) => {
    try {
      const { data } = await axios.get('https://www.cbr-xml-daily.ru/daily_json.js');
      const currency = Object.values(data.Valute)
        .filter((item) => item.CharCode === 'USD' || item.CharCode === 'EUR');
      res.json(currency);
    } catch (err) {
      res.status(500)
        .json({
          message: 'Не удалось загрузить курсы валют, попробуйте перезагрузить страницу',
          messageType: 'error',
        });
    }
  },
);

export default router;
