import express from 'express';

import Contacts from '../../models/Contacts.js';

const router = express.Router();

// @route   api/contacts/
// @desc    Get contacts
router.get(
  '/all-contacts',
  async (req, res) => {
    try {
      const contacts = await Contacts.find();
      res.send(contacts);
    } catch (err) {
      res.status(500).json({ message: 'Не удалось загрузить контактные данные', messageType: 'error' });
    }
  },
);

// @route   api/contacts/
// @desc    Update contacts
router.put(
  '/update/:id',
  async (req, res) => {
    const contacts = await Contacts.findById(req.params.id);
    try {
      if (req.body.mainPhone) {
        contacts.mainPhone = req.body.mainPhone;
      }

      if (req.body.additionalPhone) {
        contacts.additionalPhone = req.body.additionalPhone;
      }

      if (req.body.address) {
        contacts.address = req.body.address;
      }

      await contacts.save();
      res.status(201).json({ message: 'Контактные данные успешно обновлены', messageType: 'success' });
    } catch (err) {
      res.status(500).json({ message: 'Не удалось обновить контактные данные', messageType: 'error' });
    }
  },
);

export default router;
