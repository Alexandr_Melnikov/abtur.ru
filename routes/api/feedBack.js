import express from 'express';
import nodemailer from 'nodemailer';

const router = express.Router();

router.post(
  '/call-back',
  async (req, res) => {
    const output = `
      <h3>Запрос обратного звонка</h3>
      <ul>
        <li>Name: ${req.body.name}</li>
        <li>Phone: ${req.body.phone}</li>
        <li>Comment: ${req.body.comment}</li>
      </ul>`;

    try {
      const transporter = nodemailer.createTransport({
        host: 'smtp.timeweb.ru',
        port: 2525,
        secure: false,
        auth: {
          user: 'submit@avtopark-arenda.ru',
          pass: 'zaika2005',
        },
      });

      await transporter.sendMail({
        from: 'Аба Вояж <submit@avtopark-arenda.ru>',
        to: 'melnikoff.alexandr@gmail.com',
        subject: 'Тест темы письма',
        text: 'Проверка отправки почты',
        html: output,
      });

      res.json({
        message: 'Ваша заявка успешно отправлена',
        messageType: 'success',
      });
    } catch (err) {
      res.status(500)
        .json({
          message: 'Не удалось отправить заявку',
          messageType: 'error',
        });
    }
  },
);

export default router;
