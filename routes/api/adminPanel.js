import express from 'express';

import User from '../../models/User.js';

const router = express.Router();

// @route   api/user/
// @desc    Get current user
router.get(
  '/user/:id',
  async (req, res) => {
    try {
      const user = await User.findById(req.params.id)
        .select('-password');
      res.json(user);
    } catch (err) {
      res.status(500)
        .json({
          message: 'Не удалось загрузить данные пользователя',
          messageType: 'error',
        });
    }
  },
);

export default router;
