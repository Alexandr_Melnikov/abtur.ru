import express from 'express';
import config from 'config';
import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import { check, validationResult } from 'express-validator';

import User from '../../models/User.js';

const router = express.Router();

// @route   api/auth/register
// @desc    Register user
router.post(
  '/register',
  [
    check('name', 'Имя обязательно для заполнения')
      .not()
      .isEmpty(),
    check('email', 'Некорректный email')
      .isEmail(),
    check('password', 'Минимальная длинна пароля 3 символа')
      .isLength({ min: 3 }),
  ],
  async (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(400)
          .json({
            errors: errors.array(),
            message: 'Некорректные данные при регистрации',
            messageType: 'error',
          });
      }

      const {
        name,
        email,
        password,
      } = req.body;

      const candidate = await User.findOne({ email });

      if (candidate) {
        return res
          .status(400)
          .json({
            errors: [{
              message: 'Такой пользователь уже существует',
              messageType: 'error',
            }],
          });
      }

      const hashedPassword = await bcrypt.hash(password, 5);
      const user = new User({
        name,
        email,
        password: hashedPassword,
      });

      await user.save();

      res.status(201)
        .json({
          message: 'Пользователь создан',
          messageType: 'success',
        });
    } catch (err) {
      console.error(err.message);
      res.status(500)
        .json({
          message: 'Что то пошло не так, попробуйте снова',
          messageType: 'error',
        });
    }
  },
);

// @route   POST api/auth/login
// @desc    Authenticate user
router.post('/login', [
  check('email', 'Введите корректный email')
    .normalizeEmail()
    .isEmail(),
  check('password', 'Введите пароль')
    .exists(),
], async (req, res) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400)
        .json({
          errors: errors.array(),
          message: 'Некорректные данные при входе в систему',
          messageType: 'error',
        });
    }

    const {
      email,
      password,
    } = req.body;

    const user = await User.findOne({ email });

    if (!user) {
      return res.status(400)
        .json({
          message: 'Пользователь не найден',
          messageType: 'error',
        });
    }

    const isMatch = await bcrypt.compare(password, user.password);

    if (!isMatch) {
      return res.status(400)
        .json({
          message: 'Неверный пароль, попробуйте снова',
          messageType: 'error',
        });
    }

    const token = jwt.sign(
      { userId: user.id },
      config.get('jwtSecret'),
      { expiresIn: '1h' },
    );

    res.json({
      token,
      userId: user.id,
    });
  } catch (err) {
    res.status(500)
      .json({
        message: 'Что то пошло не так, попробуйте снова',
        messageType: 'error',
      });
  }
});

export default router;
