const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const TerserWebpackPlugin = require('terser-webpack-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const Dotenv = require('dotenv-webpack');

const isDev = process.env.NODE_ENV === 'development';

const getFileName = (ext) => (isDev ? `[name].${ext}` : `[name].[contenthash].${ext}`);
const getOptimization = () => {
  const config = {
    splitChunks: {
      chunks: 'all',
    },
  };

  if (!isDev) {
    config.minimizer = [
      new CssMinimizerPlugin({
        parallel: true,
      }),
      new TerserWebpackPlugin(),
    ];
  }

  return config;
};
const getPlugins = () => [
  new HtmlWebpackPlugin({
    title: 'Аба Вояж',
    filename: 'index.html',
    favicon: './assets/favicon.ico',
    template: './assets/index.html',
    minify: {
      collapseWhitespace: !isDev,
    },
  }),
  new MiniCssExtractPlugin({
    filename: getFileName('css'),
  }),
  new Dotenv({
    path: isDev ? './.env.dev' : './.env.prod',
  }),
];
const cssLoaders = (extra) => {
  const loaders = [
    {
      loader: MiniCssExtractPlugin.loader,
    },
    {
      loader: 'css-loader',
      options: {
        modules: {
          localIdentName: '[name]__[local]___[hash:base64:5]',
        },
      },
    },
  ];

  if (isDev) {
    loaders.splice(0, 1, 'style-loader');
  }

  if (extra) {
    loaders.push(extra);
  }

  return loaders;
};

module.exports = {
  context: path.resolve(__dirname, 'src'),
  mode: isDev ? 'development' : 'production',
  devtool: isDev ? 'eval-cheap-module-source-map' : false,
  entry: {
    main: ['babel-polyfill', './index.js'],
  },
  output: {
    filename: getFileName('js'),
    path: path.resolve(__dirname, 'dist'),
    assetModuleFilename: 'assets/[hash][ext]',
    clean: true,
  },
  performance: {
    hints: process.env.NODE_ENV === 'production' ? 'warning' : false,
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.jsx', 'scss'],
  },
  optimization: getOptimization(),
  devServer: {
    historyApiFallback: true,
    port: 3000,
    host: 'localhost',
    proxy: {
      '/api': {
        target: 'http://localhost:4200',
      },
    },
  },
  plugins: getPlugins(),
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: ['babel-loader'],
      },
      {
        test: /\.css$/,
        use: cssLoaders(),
      },
      {
        test: /\.s[ac]ss$/,
        use: cssLoaders('sass-loader'),
      },
      {
        test: /\.(png|svg|jpg|jpeg|gif)$/i,
        type: 'asset/resource',
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/i,
        type: 'asset/resource',
      },
    ],
  },
};
