/* import React from 'react';

import { ReactComponent as Wallet } from '../assets/image/svg/front/Advantages/wallet.svg';
import WalletIcon from '../assets/image/advantages/wallet.png';
import { ReactComponent as Percent } from '../assets/image/svg/front/Advantages/percentage.svg';
import { ReactComponent as Handshake } from '../assets/image/svg/front/Advantages/handshake.svg';
import { ReactComponent as Calendar } from '../assets/image/svg/front/Advantages/calendar.svg'; */

import GrayPng from '../assets/image/slider/gray.png';
import GreenPng from '../assets/image/slider/green.png';
import YellowPng from '../assets/image/slider/yellow.png';

import offersCardImg from '../assets/image/offersCard/offersCardImage.png';

import Veronika from '../assets/image/team/team_vera.jpg';

export const menuItem = [
  {
    title: 'Поиск туров',
    link: '/search-tour',
  },
  {
    title: 'Туры по России',
    link: '/tours-in-russia',
  },
  {
    title: 'Спецпредложения',
    link: '/special-offers',
  },
  {
    title: 'Авиабилеты',
    link: '/flights',
  },
  {
    title: 'Страны',
    link: '/countries',
  },
  {
    title: 'О компании',
    link: '/about-company',
  },
  {
    title: 'Контакты',
    link: '/contacts',
  },
  {
    title: 'Заказ тура',
    link: '/order-tour',
  },
];

export const dataCard = [
  {
    id: 1,
    title: 'Выгодные цены',
    desc: 'У нас всегда самые лучшие предложения',
    // img: WalletIcon,
  },
  {
    id: 2,
    title: 'Надёжность и честность',
    desc: 'Все цены окончательные, никаких скрытых доплат',
    // img: <Percent />,
  },
  {
    id: 3,
    title: 'Индивидуальные подход',
    desc: 'Гибкая ценовая политика для каждого клиента',
    // img: <Handshake />,
  },
  {
    id: 4,
    title: '12 лет на рынке',
    desc: '18175 довольных туристов!',
    // img: <Calendar />,
  },
];

export const imageSlider = [
  {
    id: 1,
    description: 'Изображение 1',
    link: GrayPng,
  },
  {
    id: 2,
    description: 'Изображение 2',
    link: GreenPng,
  },
  {
    id: 3,
    description: 'Изображение 3',
    link: YellowPng,
  },
];

export const specialOffers = [
  {
    id: 1,
    name: 'Тайланд',
    img: offersCardImg,
    url: 'http://www.test.ru/',
  },
  {
    id: 2,
    name: 'Египет',
    img: offersCardImg,
    url: 'http://www.test.ru/',
  },
  {
    id: 3,
    name: 'Турция',
    img: offersCardImg,
    url: 'http://www.test.ru/',
  },
  {
    id: 4,
    name: 'Мальдивы',
    img: offersCardImg,
    url: 'http://www.test.ru/',
  },
];

export const ourTeam = [
  {
    id: 1,
    name: 'Вероника Артемьева',
    post: 'Директор',
    img: Veronika,
    desc:
      'Все, что меня вдохновляет - это возможность взять билет на самолет и в течение нескольких часов оказаться там, где я никогда не была. И как говорится в голландской пословице: «Для тех, кто только что вышел за дверь, самая трудная часть осталась позади».',
  },
  {
    id: 2,
    name: 'Елена Бондаренко',
    post: 'Менеджер',
    img: Veronika,
    desc:
      'Все, что меня вдохновляет - это возможность взять билет на самолет и в течение нескольких часов оказаться там, где я никогда не была. И как говорится в голландской пословице: «Для тех, кто только что вышел за дверь, самая трудная часть осталась позади».',
  },
  {
    id: 3,
    name: 'Анастасия Берсенева',
    post: 'Менеджер',
    img: Veronika,
    desc: 'Все, что меня вдохновляет - это возможность взять билет на самолет и в течение нескольких часов оказаться там, где я никогда не была. И как говорится в голландской пословице: «Для тех, кто только что вышел за дверь, самая трудная часть осталась позади».',
  },
];

export const orderTourData = {
  count: [{ label: '1' }, { label: '2' }, { label: '3' }, { label: '4' }],
  hotelLevel: [
    { label: 'Любой' },
    { label: '2 звезды' },
    { label: '3 звезды' },
    { label: '4 звезды' },
    { label: '5 звезд' },
  ],
  foodLevel: [
    { label: 'RO - Проживание без питания' },
    { label: 'BB - Проживание с завтраком' },
    { label: 'HB - Завтрак и ужин по системе шведского стола' },
    { label: 'FB - Завтрак, обед и ужин по системе шведского стола' },
    {
      label: 'AI - Всё включено. Еда и напитки(местного производства)',
    },
    {
      label: 'UAI - Всё включено. Еда и напитки(импортного производства)',
    },
  ],
  paymentMethod: [{ label: 'За всех' }, { label: 'За одного' }],
};
