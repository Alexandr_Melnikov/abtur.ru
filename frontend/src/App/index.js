import React, { useEffect } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

import { useDispatch, useSelector } from 'react-redux';
import MainPage from '../pages/MainPage';
import SearchTourPage from '../pages/SearchTourPage';
import ToursInRussia from '../pages/ToursInRussia';
import SpecialOffersPage from '../pages/SpecialOffersPage';
import FlightsPage from '../pages/FlightsPage';
import CountriesPage from '../pages/CountriesPage';
import AboutCompanyPage from '../pages/AboutCompanyPage';
import ContactsPage from '../pages/ContactsPage';
import OrderTourPage from '../pages/OrderTourPage';
import NotFound from '../pages/NotFound';

import AdminPanel from '../Admin/AdminPanel';
import Auth from '../Admin/Auth';

import styles from './styles.scss';
import { fetchContacts } from '../store/reducers/commonReducer';

const App = () => {
  const auth = useSelector((state) => state?.auth?.items);
  const data = JSON.parse(localStorage.getItem('userData'));
  const isAuth = auth?.token || data?.token;

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchContacts());
  }, [dispatch]);

  return (
    <div className={styles.App}>
      <Switch>
        <Route exact path="/" component={MainPage} />
        <Route path="/search-tour" component={SearchTourPage} />
        <Route path="/tours-in-russia" component={ToursInRussia} />
        <Route path="/special-offers" component={SpecialOffersPage} />
        <Route path="/flights" component={FlightsPage} />
        <Route path="/countries" component={CountriesPage} />
        <Route path="/about-company" component={AboutCompanyPage} />
        <Route path="/contacts" component={ContactsPage} />
        <Route path="/order-tour" component={OrderTourPage} />

        <Route exact path="/auth">
          {isAuth ? <Redirect to="/admin-panel" /> : <Auth />}
        </Route>
        <Route exact path="/admin-panel">
          {!isAuth ? <Redirect to="/auth" /> : <AdminPanel />}
        </Route>

        <Route path="*" component={NotFound} />
      </Switch>
    </div>
  );
};

export default App;
