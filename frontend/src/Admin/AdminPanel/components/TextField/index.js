import React from 'react';
import { FormControl, InputLabel, OutlinedInput } from '@mui/material';

import styles from './styles.scss';

export const TextField = ({
  id,
  name,
  value,
  label,
  onChange,
}) => (
  <FormControl className={styles.textField}>
    <InputLabel htmlFor={id}>{label}</InputLabel>
    <OutlinedInput
      id={id}
      name={name}
      label={label}
      value={value || ''}
      onChange={(event) => onChange(event)}
    />
  </FormControl>
);
