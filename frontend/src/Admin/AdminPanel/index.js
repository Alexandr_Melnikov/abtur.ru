import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import {
  AppBar, Button, IconButton, Menu, MenuItem, Toolbar, Typography,
} from '@mui/material';
import { AccountCircleOutlined, ExitToAppOutlined } from '@mui/icons-material';

import { AdminLayout } from '../../Layout';
import { TextField } from './components/TextField';

import { logoutUser, setUserAfterRebootPage } from '../../store/reducers/authReducer';
import { getCurrentUser } from '../../store/reducers/adminPanelReducer';

import { changeInput, updateContacts } from '../../store/reducers/commonReducer';

import styles from './styles.scss';

const AdminPanel = () => {
  const [anchor, setAnchor] = useState(null);

  const dispatch = useDispatch();
  const user = useSelector((state) => state.adminPanel.user);
  const contacts = useSelector((state) => state.common.contacts);

  useEffect(() => {
    const userData = JSON.parse(localStorage.getItem('userData'));
    const { userId } = userData;
    dispatch(setUserAfterRebootPage(userData));
    dispatch(getCurrentUser(userId));
  }, [dispatch]);

  return (
    <AdminLayout>
      <AppBar position="static">
        <Toolbar className={styles.toolbar}>
          <Typography variant="h6">Управление сайтом</Typography>
          <div>
            <span className={styles.userName}>{user?.item?.name}</span>
            <IconButton onClick={(event) => setAnchor(event.currentTarget)} color="inherit">
              <AccountCircleOutlined />
            </IconButton>
            <Menu
              anchorEl={anchor}
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'left',
              }}
              transformOrigin={{
                vertical: 'top',
                horizontal: 'left',
              }}
              keepMounted
              open={Boolean(anchor)}
              onClose={() => setAnchor(null)}
            >
              <MenuItem onClick={() => dispatch(logoutUser())}>
                <ExitToAppOutlined sx={{ marginRight: '8px' }} />
                Выход
              </MenuItem>
            </Menu>
          </div>
        </Toolbar>
      </AppBar>
      <fieldset className={styles.block}>
        <legend>Контактные данные</legend>
        <div className={styles.contactsBlock}>
          <TextField
            name="mainPhone"
            value={contacts.items.mainPhone}
            label="Основной номер"
            onChange={({ target }) => dispatch(changeInput({
              field: target.name,
              value: target.value,
            }))}
          />
          <TextField
            value={contacts.items.additionalPhone}
            name="additionalPhone"
            label="Дополнительный номер"
            onChange={({ target }) => dispatch(changeInput({
              field: target.name,
              value: target.value,
            }))}
          />
          <TextField
            name="address"
            value={contacts.items.address}
            label="Адресс"
            onChange={({ target }) => dispatch(changeInput({
              field: target.name,
              value: target.value,
            }))}
          />
          <Button variant="contained" onClick={() => dispatch(updateContacts(contacts.items))}>Обновить данные</Button>
        </div>
      </fieldset>
    </AdminLayout>
  );
};

export default AdminPanel;
