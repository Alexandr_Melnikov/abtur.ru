import React from 'react';

import { useDispatch, useSelector } from 'react-redux';

import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup';

import {
  Box, Button, Card, CardContent, CircularProgress, TextField, Typography,
} from '@mui/material';

import { AdminLayout } from '../../Layout';

import { loginUser } from '../../store/reducers/authReducer';

import SignInIcon from '../../assets/image/svg/admin/sign-in';

import styles from './styles.scss';

const authSchema = Yup.object()
  .shape({
    email: Yup.string()
      .email('Не валидный email')
      .required('Поле обязательно к заполнению'),
    password: Yup.string()
      .min(6, 'Пароль должен состоять минимум из 6 цифр')
      .matches(/[a-zA-Z1-9]/, 'Пароль должен содержать только латинские символы или цифры')
      .required('Поле обязательно к заполнению'),
  });

const Auth = () => {
  const auth = useSelector((state) => state.auth);
  const dispatch = useDispatch();
  const {
    register,
    handleSubmit,
    formState: {
      isDirty,
      isValid,
      errors,
    },
  } = useForm({
    mode: 'onChange',
    resolver: yupResolver(authSchema),
  });

  return (
    <AdminLayout>
      <Card className={styles.auth} raised>
        <CardContent>
          <Typography variant="h5">Авторизация</Typography>
          <form onSubmit={handleSubmit(({
            email,
            password,
          }) => dispatch(loginUser({
            email,
            password,
          })))}
          >
            <Box className={styles.formContent}>
              <TextField {...register('email')} label="Email" placeholder="my-mail@gmail.com" />
              {errors.email && (
                <Typography variant="subtitle2" color="error">
                  {errors.email.message}
                </Typography>
              )}
              <TextField {...register('password')} label="Пароль" type="password" />
              {errors.password && (
                <Typography variant="subtitle2" color="error">
                  {errors.password.message}
                </Typography>
              )}
            </Box>
            {auth.loading ? (
              <div className={styles.loaderContainer}>
                <CircularProgress size={34} />
              </div>
            ) : (
              <Button type="submit" variant="contained" color="primary" disabled={!isDirty || !isValid}>
                Войти
                <SignInIcon />
              </Button>
            )}
          </form>
        </CardContent>
      </Card>
    </AdminLayout>
  );
};

export default Auth;
