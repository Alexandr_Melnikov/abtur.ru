import React from 'react';

import { Container, CssBaseline } from '@mui/material';

import Header from '../Components/Header';
import Menu from '../Components/Menu';
import Footer from '../Components/Footer';

import Breadcrumbs from '../Components/Breadcrumbs';
import Notify from '../Components/Notify';

export const MainLayout = ({ children }) => (
  <Container>
    <CssBaseline />
    <Notify />
    <Header />
    <Menu />
    <Breadcrumbs />
    {children}
    <Footer />
  </Container>
);

export const AdminLayout = ({ children }) => (
  <Container>
    <CssBaseline />
    <Notify />
    {children}
  </Container>
);
