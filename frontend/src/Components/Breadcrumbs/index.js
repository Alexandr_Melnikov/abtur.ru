import React from 'react';
import { useHistory } from 'react-router-dom';

import { Breadcrumbs, Link } from '@mui/material';
import { NavigateNext } from '@mui/icons-material';

import styles from './styles.scss';

const BreadCrumbs = () => {
  const history = useHistory();

  let breadCrumbs = [{ label: 'Главная страница', link: '/' }];
  switch (history.location.pathname) {
    case '/search-tour':
      breadCrumbs.push({ label: 'Поиск туров', link: '/search-tour' });
      break;
    case '/tours-in-russia':
      breadCrumbs.push({ label: 'Туры по России', link: '/tours-in-russia' });
      break;
    case '/special-offers':
      breadCrumbs.push({
        label: 'Специальные предложения',
        link: '/special-offers',
      });
      break;
    case '/flights':
      breadCrumbs.push({ label: 'Авиабилеты', link: '/flights' });
      break;
    case '/countries':
      breadCrumbs.push({ label: 'Страны', link: '/countries' });
      break;
    case '/about-company':
      breadCrumbs.push({ label: 'О компании', link: '/about-company' });
      break;
    case '/contacts':
      breadCrumbs.push({ label: 'Контакты', link: '/contacts' });
      break;
    case '/order-tour':
      breadCrumbs.push({ label: 'Заказ тура', link: '/order-tour' });
      break;
    default:
      breadCrumbs = [{ label: 'Главная страница', link: '/' }];
      break;
  }

  return (
    <Breadcrumbs separator={<NavigateNext />} className={styles.Breadcrumbs}>
      {breadCrumbs.map(({ link, label }, index) => (
        breadCrumbs.length >= 2 && (
        <Link
          className={index === breadCrumbs.length - 1 ? styles.ActiveLink : styles.Link}
          key={label}
          href={link}
          onClick={() => history.push(link)}
        >
          {label}
        </Link>
        )
      ))}
    </Breadcrumbs>
  );
};

export default BreadCrumbs;
