import React from 'react';
import { useSelector } from 'react-redux';

import PhoneIcon from '../../assets/image/svg/front/phone';

import styles from './styles.scss';

const Phone = ({
  withIcon,
  phone,
}) => (
  <div className={styles.Phone}>
    {withIcon && <PhoneIcon width="20px" height="20px" />}
    <a href={`tel:+${phone}`}>{phone}</a>
  </div>
);

const Contacts = ({ withIcon }) => {
  const contacts = useSelector((state) => state.common.contacts);

  return (
    <div className={styles.Contacts}>
      <div className={styles.PhoneGroup}>
        <Phone withIcon={withIcon} phone={contacts.items.mainPhone} />
        <Phone withIcon={withIcon} phone={contacts.items.additionalPhone} />
      </div>
    </div>
  );
};

export default Contacts;
