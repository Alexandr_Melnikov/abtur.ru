import React, { useState } from 'react';

import { Link } from 'react-router-dom';

import { Button, Grid } from '@mui/material';
import { Box } from '@mui/system';
import Modal from '../Modal';

import Logo from '../../assets/image/logo.png';

import PhoneIcon from '../../assets/image/svg/front/phone';
import ShoppingCartIcon from '../../assets/image/svg/front/shopping-cart';

import styles from './styles.scss';

const Header = () => {
  const [openModal, setOpenModal] = useState(false);
  const closeModalDialog = () => {
    setOpenModal(false);
  };
  const openModalDialog = () => {
    setOpenModal(true);
  };

  return (
    <>
      <Grid container className={styles.Header}>
        <Grid item xs={6}>
          <Modal open={openModal} closeModal={closeModalDialog} />
          <Link to="/">
            <div className={styles.Logo}>
              <Box sx={{
                display: {
                  xs: 'none',
                  md: 'block',
                },
              }}
              >
                <img src={Logo} alt="logo" />
              </Box>
              <div className={styles.Logo__text}>
                <h1>Аба Вояж</h1>
                <div className={styles.Line} />
                <h3>Ваше турагенство</h3>
              </div>
            </div>
          </Link>
        </Grid>

        <Grid item xs={6} className={styles.Contacts}>
          <div className={styles.ButtonGroup}>
            <Button variant="contained" className={styles.firstButton}>
              Заказ тура
              <ShoppingCartIcon width="24px" height="24px" />
            </Button>
            <Button variant="contained" onClick={openModalDialog}>
              Обратный звонок
              <PhoneIcon width="24px" height="24px" />
            </Button>
          </div>
          {/* <Box sx={{ display: { xs: 'none', md: 'block' } }}> */}
          {/*  <Contacts /> */}
          {/* </Box> */}
        </Grid>
      </Grid>
    </>
  );
};

export default Header;
