import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { Grid, Tooltip } from '@mui/material';

import { TrendingUpOutlined, TrendingFlatOutlined, TrendingDownOutlined } from '@mui/icons-material';

import { fetchCurrency } from '../../store/reducers/siteReducer';

import EuroFlagIcon from '../../assets/image/svg/front/eu-flag';
import UsaFlagIcon from '../../assets/image/svg/front/us-flag';

import styles from './styles.scss';

const Currency = () => {
  const dispatch = useDispatch();
  const currency = useSelector((state) => state.site.currency);

  useEffect(() => {
    dispatch(fetchCurrency());
  }, [dispatch]);

  return (
    <div className={styles.Currency}>
      {currency.items.map((item) => {
        const {
          ID: id,
          CharCode: currencyCode,
          Previous: previousCourse,
          Value: currentCourse,
        } = item;
        return (
          <Grid item className={styles.CurrencyItem} key={id}>
            {currencyCode === 'USD' ? <UsaFlagIcon /> : <EuroFlagIcon />}
            <span>
              {parseFloat(currentCourse)
                .toFixed(2)}
            </span>
            {currentCourse > previousCourse && (
              <Tooltip title={`Выше чем вчера на ${(currentCourse - previousCourse).toFixed(2)}`} placement="top">
                <TrendingUpOutlined className={styles.ArrowUp} />
              </Tooltip>
            )}
            {currentCourse < previousCourse && (
              <Tooltip title={`Ниже чем вчера на ${(previousCourse - currentCourse).toFixed(2)}`} placement="top">
                <TrendingDownOutlined className={styles.ArrowDown} />
              </Tooltip>
            )}
            {currentCourse === previousCourse && (
              <Tooltip title="Так же как вчера" placement="top">
                <TrendingFlatOutlined className={styles.ArrowEqually} />
              </Tooltip>
            )}
          </Grid>
        );
      })}
    </div>
  );
};

export default Currency;
