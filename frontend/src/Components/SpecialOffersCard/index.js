import React from 'react';

import { Grid, Box } from '@mui/material';

import styles from './styles.scss';

const SpecialOffersCard = ({ name, img, url }) => (
  <Grid container>
    <div className={styles.Card}>
      <img src={img} alt={name} />
      <Box className={styles.CardTitle}>
        <a href={url}>{name}</a>
      </Box>
    </div>
  </Grid>
);

export default SpecialOffersCard;
