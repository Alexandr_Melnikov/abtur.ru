import React from 'react';
import { Link } from 'react-router-dom';
import { Grid } from '@mui/material';
// import { Box } from '@mui/system';

import Contacts from '../Contacts';

/* import { ReactComponent as VisaIcon } from '../../assets/image/svg/social/visa.svg';
import { ReactComponent as MasterCardIcon } from '../../assets/image/svg/social/mastercard.svg';
import { ReactComponent as FaceBookIcon } from '../../assets/image/svg/social/facebook.svg';
import { ReactComponent as InstagramIcon } from '../../assets/image/svg/social/instagram.svg';
import { ReactComponent as TelegramIcon } from '../../assets/image/svg/social/telegram.svg';
import { ReactComponent as ViberIcon } from '../../assets/image/svg/social/viber.svg';
import { ReactComponent as VkIcon } from '../../assets/image/svg/social/vk.svg';
import { ReactComponent as WhatsAppIcon } from '../../assets/image/svg/social/whatsapp.svg'; */

import styles from './styles.scss';

const Footer = () => (
  <Grid container className={styles.Footer}>
    <Grid container>
      <Grid container alignItems="center" className={styles.FooterTop}>
        <div className={styles.FooterTopMenu}>
          {/* <div>
              <h3>Туристическое агентство «Аба Вояж», 2019</h3>
              <h3>Разработка сайта — Melnikov Alexandr</h3>
            </div> */}
          <ul>
            <li>
              <Link to="/search-tour">Поиск туров</Link>
            </li>

            <li>
              <Link to="/tours-in-russia">Туры по России</Link>
            </li>
            <li>
              <Link to="/special-offers">Спецпредложения</Link>
            </li>
          </ul>
          <ul>
            <li>
              <Link to="/сountries">Страны</Link>
            </li>

            <li>
              <Link to="/about-company">О компании</Link>
            </li>
            <li>
              <Link to="/contacts">Контакты</Link>
            </li>
          </ul>
        </div>
        <div>
          <Contacts withIcon />
        </div>
      </Grid>
      <Grid container className={styles.FooterBottom}>
        {/* <Grid item className={styles.SocialIcon}>
           <FaceBookIcon width="50px" height="50px" />
          <InstagramIcon width="50px" height="50px" />
          <TelegramIcon width="50px" height="50px" />
          <ViberIcon width="50px" height="50px" />
          <VkIcon width="50px" height="50px" />
          <WhatsAppIcon width="50px" height="50px" />
        </Grid> */}
        {/* <Grid item className={styles.CreditCardIcon}>
           <VisaIcon width="80px" height="80px" />
          <MasterCardIcon width="80px" height="80px" />
        </Grid> */}
      </Grid>
    </Grid>
  </Grid>
);

export default Footer;
