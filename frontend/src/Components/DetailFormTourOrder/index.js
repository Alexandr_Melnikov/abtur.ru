import React, { useState } from 'react';
import {
  Autocomplete, Box, Button, Slider, TextField, Typography,
} from '@mui/material';

import { DateRangePicker, LocalizationProvider } from '@mui/lab';
import AdapterDayjs from '@mui/lab/AdapterDayjs';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup';
import dayjs from 'dayjs';
import { orderTourData } from '../../utils/data';
import Caption from '../Caption';
import 'dayjs/locale/ru';

import styles from './styles.scss';

const detailFormOrderTourSchema = Yup.object().shape({
  from: Yup.string().min(3, 'Не короче 3 символов').required('Поле обязательно к заполнению'),
  to: Yup.string().min(3, 'Не короче 3 символов').required('Поле обязательно к заполнению'),
  name: Yup.string().min(3, 'Имя должно быть не короче 3 символов').required('Поле обязательно к заполнению'),
  phone: Yup.string().min(6, 'Телефон должен состоять минимум из 6 цифр').required('Поле обязательно к заполнению'),
});

const DetailFormTourOrder = () => {
  dayjs.locale('ru');
  const [tourInfo, setTourInfo] = useState({
    flightDates: [null, null],
    duration: [7, 23],
    price: [60000, 150000],
  });

  const changeField = (field, value) => {
    const newState = { ...tourInfo };
    newState[field] = value;
    setTourInfo(newState);
  };

  const {
    register,
    handleSubmit,
    reset,
    formState: { isDirty, isValid },
  } = useForm({
    resolver: yupResolver(detailFormOrderTourSchema),
  });

  const submitForm = ({
    from,
    to,
    adults,
    children,
    startDate,
    endDate,
    hotelLevel,
    foodLevel,
    paymentMethod,
    name,
    phone,
    email,
    comment,
  }) => {
    const { duration, price } = tourInfo;
    reset();
    console.log({
      from,
      to,
      adults,
      children,
      startDate,
      endDate,
      duration,
      hotelLevel,
      foodLevel,
      price,
      paymentMethod,
      name,
      phone,
      email,
      comment,
    });
  };

  return (
    <div className={styles.DetailFormTourOrder}>
      <Caption title="Подробная заявка" variant="h4" mt={2} />
      <form onSubmit={handleSubmit(submitForm)}>
        <Box className={styles.formContent}>
          <TextField
            {...register('from')}
            className={styles.FormInput}
            label="Город вылета"
            required
            autoComplete="nope"
          />
          <TextField
            {...register('to')}
            className={styles.FormInput}
            name="to"
            label="Страна назначения"
            required
            autoComplete="nope"
          />
          <Box className={styles.PeopleCount}>
            <Autocomplete
              style={{ width: 150 }}
              options={orderTourData.count}
              getOptionLabel={(option) => option.label}
              renderInput={(params) => (
                <TextField
                  {...params}
                  {...register('adults')}
                  className={styles.FormInput}
                  label="Взрослые"
                />
              )}
            />
            <Autocomplete
              style={{ width: 150 }}
              options={orderTourData.count}
              getOptionLabel={(option) => option.label}
              renderInput={(params) => (
                <TextField
                  {...params}
                  {...register('children')}
                  className={styles.FormInput}
                  label="Дети"
                />
              )}
            />
          </Box>
          <LocalizationProvider dateAdapter={AdapterDayjs}>
            <DateRangePicker
              startText="С какого числа"
              endText="По какое число"
              mask="__.__.____"
              disablePast
              value={tourInfo.flightDates}
              onChange={(date) => changeField('flightDates', date)}
              renderInput={(startProps, endProps) => (
                <div className={styles.datePickerWrapper}>
                  <TextField
                    {...startProps}
                    {...register('startDate')}
                    className={styles.FormDateInput}
                    helperText={null}
                  />
                  <TextField
                    {...endProps}
                    {...register('endDate')}
                    className={styles.FormDateInput}
                    helperText={null}
                  />
                </div>
              )}
            />
          </LocalizationProvider>
          <Typography>{`Количество ночей от ${tourInfo.duration[0]} до ${tourInfo.duration[1]}`}</Typography>
          <Slider
            valueLabelDisplay="auto"
            className={styles.FormInput}
            max={30}
            value={tourInfo.duration}
            onChange={(event, value) => changeField('duration', value)}
          />
          <Autocomplete
            style={{ width: 150 }}
            options={orderTourData.hotelLevel}
            getOptionLabel={(option) => option.label}
            renderInput={(params) => (
              <TextField
                {...params}
                {...register('hotelLevel')}
                className={styles.FormInput}
                label="Класс отеля"
              />
            )}
          />
          <Autocomplete
            options={orderTourData.foodLevel}
            getOptionLabel={(option) => option.label}
            renderInput={(params) => (
              <TextField
                {...params}
                {...register('foodLevel')}
                className={styles.FormInput}
                label="Питание"
              />
            )}
          />
          <Box className={styles.PriceMethod}>
            <Box className={styles.PriceMethodSlider}>
              <Typography>{`Цена от ${tourInfo.price[0]} руб. до ${tourInfo.price[1]} руб.`}</Typography>
              <Slider
                valueLabelDisplay="off"
                max={200000}
                value={tourInfo.price}
                onChange={(event, value) => changeField('price', value)}
                step={5000}
              />
            </Box>
            <Autocomplete
              className={styles.peoplePaymentCount}
              options={orderTourData.paymentMethod}
              getOptionLabel={(option) => option.label}
              renderInput={(params) => (
                <TextField
                  {...params}
                  {...register('paymentMethod')}
                  className={styles.FormInput}
                  label="Оплата"
                />
              )}
            />
          </Box>
          <TextField className={styles.FormInput} {...register('name')} label="Ваше имя" required />
          <TextField className={styles.FormInput} {...register('phone')} label="Номер телефона" required />
          <TextField className={styles.FormInput} {...register('email')} label="E-mail" />
          <TextField className={styles.FormInput} {...register('comment')} label="Комментарий" autoComplete="nope" />
          <Box>
            <Button
              type="submit"
              variant="contained"
              color="primary"
              disabled={!isDirty || !isValid}
            >
              Отправить заявку
            </Button>
          </Box>
        </Box>
      </form>
    </div>
  );
};

export default DetailFormTourOrder;
