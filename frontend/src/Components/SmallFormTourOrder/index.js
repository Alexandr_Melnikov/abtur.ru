import React from 'react';
import {
  Button, TextField, Typography, Box,
} from '@mui/material';
import * as Yup from 'yup';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import Caption from '../Caption';
import styles from './styles.scss';

const smallFormOrderTourSchema = Yup.object().shape({
  text: Yup.string().min(20, 'Опишите свой тур, минимум 20 символов').required('Поле обязательно к заполнению'),
  phone: Yup.string().min(6, 'Телефон должен состоять минимум из 6 цифр').required('Поле обязательно к заполнению'),
});

const SmallFormTourOrder = () => {
  const {
    register,
    handleSubmit,
    reset,
    formState: { isDirty, isValid, errors },
  } = useForm({
    resolver: yupResolver(smallFormOrderTourSchema),
  });

  const submitForm = ({ text, phone }) => {
    reset();
    console.log({ text, phone });
  };

  return (
    <div className={styles.SmallFormTourOrder}>
      <Caption title="Быстрая заявка" variant="h4" mt={2} />
      <form onSubmit={handleSubmit(submitForm)}>
        <Box className={styles.formContent}>
          <TextField
            {...register('text')}
            className={styles.FormInput}
            label="Опишите свой тур"
            multiline
            placeholder="Хотим пляжный отдых, вылететь через 2 недели на 7-10 ночей, отели от 4 звезд, питание всё включено..."
          />
          {errors.text && (
          <Typography variant="subtitle2" color="error">
            {errors.text.message}
          </Typography>
          )}
          <TextField {...register('phone')} className={styles.FormInput} label="Номер телефона" placeholder="8 (922) 478-91-15" />
          {errors.phone && (
          <Typography variant="subtitle2" color="error">
            {errors.phone.message}
          </Typography>
          )}
        </Box>
        <Box>
          <Button type="submit" variant="contained" color="primary" disabled={!isDirty || !isValid}>
            Отправить заявку
          </Button>
        </Box>
      </form>
    </div>
  );
};

export default SmallFormTourOrder;
