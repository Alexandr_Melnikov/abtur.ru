import React, { useEffect, useState } from 'react';

import {
  Alert, Slide, Snackbar, Stack,
} from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';
import { clearNotify } from '../../store/reducers/commonReducer';

const Notify = () => {
  const [openSnack, setOpenSnack] = useState(false);
  const dispatch = useDispatch();
  const notify = useSelector((state) => state?.common.notify);

  useEffect(() => {
    setOpenSnack(true);
  }, [notify]);

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnack(false);

    setTimeout(() => {
      dispatch(clearNotify());
    }, 500);
  };

  return (
    <Stack
      spacing={7}
    >
      {notify?.map(({
        id,
        messageType,
        message,
      }) => (
        <Snackbar
          key={id}
          autoHideDuration={5000}
          open={openSnack}
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'right',
          }}
          TransitionComponent={Slide}
          onClose={handleClose}
        >
          <Alert severity={messageType} variant="filled">{message}</Alert>
        </Snackbar>
      ))}
    </Stack>
  );
};

export default Notify;
