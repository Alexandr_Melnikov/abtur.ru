import React from 'react';
import { Typography } from '@mui/material';

import classes from './styles.scss';

const AdvantagesCard = ({
  img, title, desc,
}) => (
  <div className={classes.Card}>
    <img src={img} alt="" />
    <Typography align="center" variant="overline">
      {title}
    </Typography>
    <Typography align="center" variant="caption">
      {desc}
    </Typography>
  </div>
);

export default AdvantagesCard;
