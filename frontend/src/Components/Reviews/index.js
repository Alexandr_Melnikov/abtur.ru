import React from 'react';

// import { Grid } from ''@mui/material';
import { Typography } from '@mui/material';
import { Box } from '@mui/system';
import Caption from '../Caption';

import styles from './styles.scss';

import { ourTeam } from '../../utils/data';

// TODO Вынести стили!

const Reviews = () => (
  <>
    <Caption title="Отзывы" variant="h5" align="center" />
    {ourTeam.map((item) => (
      <div className={styles.Reviews} key={item.id}>
        <div className={styles.ReviewsInfo}>
          <div className={styles.ReviewsPhoto}>
            <img src={item.img} alt="#" />
          </div>
          <div className={styles.ReviewsDesc}>
            <Typography variant="subtitle1">{item.name}</Typography>
            <Typography variant="subtitle2">{item.post}</Typography>
          </div>
        </div>
        <Box p={2}>
          <Typography>{item.desc}</Typography>
        </Box>
      </div>
    ))}
  </>
);

export default Reviews;
