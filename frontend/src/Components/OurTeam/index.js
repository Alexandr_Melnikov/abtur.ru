import React from 'react';

//
import { Typography } from '@mui/material';
import { Box } from '@mui/system';
import Caption from '../Caption';

import styles from './styles.scss';

import { ourTeam } from '../../utils/data';

const OurTeam = () => (
  <>
    <Caption title="Наша команда" variant="h5" align="center" />
    {ourTeam.map((item) => (
      <div className={styles.Person} key={item.id}>
        <div className={styles.PersonInfo}>
          <div className={styles.PersonPhoto}>
            <img src={item.img} alt="" />
          </div>
          <div>
            <Typography variant="subtitle1">{item.name}</Typography>
            <Typography variant="subtitle2">{item.post}</Typography>
          </div>
        </div>
        <Box p={2}>
          <Typography>{item.desc}</Typography>
        </Box>
      </div>
    ))}
  </>
);

export default OurTeam;
