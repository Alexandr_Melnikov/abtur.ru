import React, { useRef } from 'react';
import { ArrowBackIosOutlined, ArrowForwardIosOutlined } from '@mui/icons-material';
import Slider from 'react-slick';

import { imageSlider } from '../../utils/data';

import styles from './styles.scss';

const ImageSlider = () => {
  const slider = useRef(null);

  return (
    <div className={styles.carouselWrapper}>
      <Slider
        ref={slider}
        dots={false}
        speed={500}
        arrows={false}
        infinite
      >
        {imageSlider.map(({ id, link, description }) => (
          <div key={id} className={styles.imageWrapper}>
            <img src={link} />
            <div className={styles.imageDescriptions}>{description}</div>
          </div>
        ))}
      </Slider>
      <div
        className={styles.sliderPrevArrow}
        onClick={() => slider.current.slickPrev()}
        aria-hidden
      >
        <ArrowBackIosOutlined fontSize="large" />
      </div>
      <div
        className={styles.sliderNextArrow}
        onClick={() => slider.current.slickNext()}
        aria-hidden
      >
        <ArrowForwardIosOutlined fontSize="large" />
      </div>
    </div>
  );
};

export default ImageSlider;
