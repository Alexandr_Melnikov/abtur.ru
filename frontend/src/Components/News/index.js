import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Dotdotdot from 'react-dotdotdot';

import dayjs from 'dayjs';
import { Box } from '@mui/system';

import Caption from '../Caption';
import Currency from '../Currency';

import { fetchNews } from '../../store/reducers/siteReducer';

import styles from './styles.scss';

const News = () => {
  const dispatch = useDispatch();
  const news = useSelector((state) => state.site.news);

  useEffect(() => {
    dispatch(fetchNews());
  }, [dispatch]);

  return (
    <>
      <Box className={styles.News}>
        <Box className={styles.Caption}>
          <Caption title={news.error ? 'Курсы валют' : 'Новости'} variant="h5" px={1} />
          <Currency />
        </Box>
        {news?.items.map((item) => (
          <a key={item.pubDate} target="_blank" rel="noopener noreferrer" href={item.link}>
            <p>
              {dayjs(item.pubDate)
                .format('DD.MM')}
            </p>
            <Dotdotdot clamp={1} tagName="span">
              {item.title}
            </Dotdotdot>
          </a>
        ))}
      </Box>
    </>
  );
};

export default News;
