import React from 'react';

import { Typography } from '@mui/material';
import { Box } from '@mui/system';

// import styles from './Caption.scss';

const Caption = ({
  my, mx, mt, mb, py, px, title, align, variant,
}) => (
  <Box my={my} mx={mx} py={py} px={px} mt={mt} mb={mb}>
    <Typography align={align} variant={variant}>
      {title}
    </Typography>
  </Box>
);

export default Caption;
