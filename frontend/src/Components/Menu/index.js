import React from 'react';
import { NavLink } from 'react-router-dom';

import { Grid } from '@mui/material';

import { menuItem } from '../../utils/data';

import styles from './styles.scss';

const Menu = () => (
  <Grid container className={styles.Menu}>
    {menuItem.map((item) => (
      <div className={styles.MenuItemWrapper} key={item.link}>
        <NavLink to={item.link} className={styles.MenuItem} activeClassName={styles.MenuItemActive}>
          {item.title}
        </NavLink>
      </div>
    ))}
  </Grid>
);

export default Menu;
