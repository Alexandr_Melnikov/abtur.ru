import React from 'react';
import {
  Box, Button, Dialog, DialogActions, DialogTitle, Grid, Slide, TextField, Typography,
} from '@mui/material';

import { useForm } from 'react-hook-form';

import { useDispatch } from 'react-redux';

import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import Caption from '../Caption';

import styles from './styles.scss';
import { callBack } from '../../store/reducers/feedBackReducer';

const feedbackSchema = yup.object()
  .shape({
    name: yup.string()
      .min(3, 'Имя должно быть не короче 3 символов')
      .required('Поле обязательно к заполнению'),
    phone: yup.string()
      .min(6, 'Телефон должен состоять минимум из 6 цифр')
      .required('Поле обязательно к заполнению'),
  });

const ModalDialog = ({
  closeModal,
  open,
}) => {
  const dispatch = useDispatch();
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
    clearErrors,
  } = useForm({
    resolver: yupResolver(feedbackSchema),
  });

  const submitForm = (data) => {
    dispatch(callBack(data));
    closeModal();
    reset();
  };

  return (
    <Dialog
      open={open}
      keepMounted
      onClose={() => {
        closeModal();
        clearErrors();
      }}
      TransitionComponent={Slide}
    >
      <DialogTitle>Заказ обратного звонка</DialogTitle>
      <Caption
        px={3}
        title="Оставьте Ваш телефон и наш специалист свяжется с Вами в ближайшее
          время"
      />
      <Grid container className={styles.ModalContent}>
        <form onSubmit={handleSubmit(submitForm)}>
          <Box className={styles.formContent}>
            <TextField {...register('name')} label="Ваше Имя" />
            {errors.name && (
              <Typography variant="subtitle2" color="error">
                {errors.name.message}
              </Typography>
            )}
            <TextField {...register('phone')} label="Ваш номер телефона" />
            {errors.phone && (
              <Typography variant="subtitle2" color="error">
                {errors.phone.message}
              </Typography>
            )}
            <TextField {...register('comment')} label="Комментарий" />
          </Box>
          <DialogActions>
            <Button
              onClick={() => {
                closeModal();
                clearErrors();
              }}
              variant="contained"
              color="primary"
            >
              Отмена
            </Button>
            <Button type="submit" variant="contained" color="primary">
              Отправить
            </Button>
          </DialogActions>
        </form>
      </Grid>
    </Dialog>
  );
};

export default ModalDialog;
