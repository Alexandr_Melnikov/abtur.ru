import { createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';
import { showNotify } from './commonReducer';

export const callBack = createAsyncThunk(
  'feedback/callback',
  async (data, {
    rejectWithValue,
    dispatch,
  }) => {
    try {
      const response = await axios.post(`${process.env.FEEDBACK}/call-back`, data);
      dispatch(showNotify(response.data));
      return response.data;
    } catch (err) {
      dispatch(showNotify(err.response.data));
      return rejectWithValue(err.response.data);
    }
  },
);
