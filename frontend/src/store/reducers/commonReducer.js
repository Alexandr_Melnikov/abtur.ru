import { createAction, createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import { nanoid } from 'nanoid';

export const showNotify = createAction('showNotify', (payload) => ({
  payload,
}));

export const fetchContacts = createAsyncThunk(
  'site/fetchContacts',
  async (_, {
    rejectWithValue,
    dispatch,
  }) => {
    try {
      const response = await axios.get(`${process.env.CONTACTS}/all-contacts`);
      return response.data;
    } catch (err) {
      dispatch(showNotify(err.response.data));
      return rejectWithValue(err.response.data);
    }
  },
);

export const updateContacts = createAsyncThunk(
  'site/updateContacts',
  async (data, {
    rejectWithValue,
    dispatch,
  }) => {
    const {
      _id: id,
      address,
      mainPhone,
      additionalPhone,
    } = data;

    try {
      const response = await axios.put(`${process.env.CONTACTS}/update/${id}`, {
        address,
        mainPhone,
        additionalPhone,
      });
      dispatch(showNotify(response.data));
      return response.data;
    } catch (err) {
      dispatch(showNotify(err.response.data));
      return rejectWithValue(err.response.data);
    }
  },
);

const commonSlice = createSlice({
  name: 'common',
  initialState: {
    contacts: {
      loading: false,
      items: {},
    },
    notify: [],
  },
  reducers: {
    changeInput: (state, action) => {
      state.contacts.items[action.payload.field] = action.payload.value;
    },
    clearNotify: (state) => {
      state.notify = [];
    },
  },
  extraReducers: (builder) => {
    builder.addCase(fetchContacts.fulfilled, (state, action) => {
      const [contacts] = action.payload;
      state.contacts.items = contacts;
    });
    builder.addCase(showNotify, (state, action) => {
      state.notify.push({ id: nanoid(5), ...action.payload });
    });
  },
});

export const {
  changeInput,
  clearNotify,
} = commonSlice.actions;
export default commonSlice.reducer;
