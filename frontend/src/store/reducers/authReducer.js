import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import { showNotify } from './commonReducer';

export const loginUser = createAsyncThunk(
  'admin-panel/auth',
  async ({
    email,
    password,
  }, {
    rejectWithValue,
    dispatch,
  }) => {
    try {
      const response = await axios.post(process.env.AUTH, {
        email,
        password,
      });
      return response.data;
    } catch (err) {
      dispatch(showNotify(err.response.data));
      return rejectWithValue(err.response.data);
    }
  },
);

const authSlice = createSlice({
  name: 'admin-panel',
  initialState: {
    loading: false,
    items: null,
  },
  reducers: {
    logoutUser: (state) => {
      localStorage.removeItem('userData');
      state.items = null;
    },
    setUserAfterRebootPage: (state, action) => {
      state.items = action.payload;
    },
  },
  extraReducers: {
    [loginUser.pending]: (state) => {
      state.loading = true;
      state.items = null;
    },
    [loginUser.fulfilled]: (state, action) => {
      const { payload } = action;
      localStorage.setItem('userData', JSON.stringify(payload));
      state.loading = false;
      state.items = payload;
    },
    [loginUser.rejected]: (state) => {
      state.loading = false;
      state.items = null;
    },
  },
});

export const {
  setUserAfterRebootPage,
  logoutUser,
} = authSlice.actions;
export default authSlice.reducer;
