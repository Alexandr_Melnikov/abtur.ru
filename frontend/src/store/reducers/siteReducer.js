import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import { showNotify } from './commonReducer';

export const fetchNews = createAsyncThunk(
  'site/fetchNews',
  async (_, {
    rejectWithValue,
    dispatch,
  }) => {
    try {
      const response = await axios.get(`${process.env.COMMON}/news`);
      return response.data;
    } catch (err) {
      dispatch(showNotify(err.response.data));
      return rejectWithValue(err.response.data);
    }
  },
);

export const fetchCurrency = createAsyncThunk(
  'site/fetchCurrency',
  async (_, {
    rejectWithValue,
    dispatch,
  }) => {
    try {
      const response = await axios.get(`${process.env.COMMON}/currency`);
      return response.data;
    } catch (err) {
      dispatch(showNotify(err.response.data));
      return rejectWithValue(err.response.data);
    }
  },
);

const siteSlice = createSlice({
  name: 'site',
  initialState: {
    news: {
      loading: false,
      items: [],
    },
    currency: {
      loading: false,
      items: [],
    },
  },
  reducers: {},
  extraReducers: {
    [fetchNews.pending]: (state) => {
      state.news.loading = true;
      state.news.items = [];
    },
    [fetchNews.fulfilled]: (state, action) => {
      state.news.loading = false;
      state.news.items = action.payload;
    },
    [fetchNews.rejected]: (state) => {
      state.news.loading = false;
      state.news.items = [];
    },
    [fetchCurrency.pending]: (state) => {
      state.currency.loading = true;
      state.currency.items = [];
    },
    [fetchCurrency.fulfilled]: (state, action) => {
      state.currency.loading = false;
      state.currency.items = action.payload;
    },
    [fetchCurrency.rejected]: (state) => {
      state.currency.loading = false;
      state.currency.items = [];
    },
  },
});

export default siteSlice.reducer;
