import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import { showNotify } from './commonReducer';

export const getCurrentUser = createAsyncThunk(
  'admin-panel/getCurrentUser',
  async (userId, {
    rejectWithValue,
    dispatch,
  }) => {
    try {
      const response = await axios.get(`${process.env.ADMIN_PANEL}/user/${userId}`);
      return response.data;
    } catch (err) {
      dispatch(showNotify(err.response.data));
      return rejectWithValue(err.response.data);
    }
  },
);

const authSlice = createSlice({
  name: 'admin-panel',
  initialState: {
    user: {
      loading: false,
      item: null,
      error: null,
    },
  },
  reducers: {},
  extraReducers: {
    [getCurrentUser.pending]: (state) => {
      state.user.loading = true;
      state.user.auth = null;
    },
    [getCurrentUser.fulfilled]: (state, action) => {
      state.user.loading = false;
      state.user.item = action.payload;
    },
    [getCurrentUser.rejected]: (state, action) => {
      console.log(action);
      state.user.loading = false;
      state.user.auth = null;
    },
  },
});

export default authSlice.reducer;
