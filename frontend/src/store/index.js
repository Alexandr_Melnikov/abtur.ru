import { configureStore } from '@reduxjs/toolkit';

import siteReducer from './reducers/siteReducer';
import authReducer from './reducers/authReducer';
import adminPanelReducer from './reducers/adminPanelReducer';
import commonReducer from './reducers/commonReducer';

const isAdminRoute = window.location.pathname === '/admin-panel' || window.location.pathname === '/auth';

const store = configureStore({
  reducer: {
    common: commonReducer,
    site: !isAdminRoute && siteReducer,
    auth: isAdminRoute && authReducer,
    adminPanel: isAdminRoute && adminPanelReducer,
  },
  middleware: (defaultMiddleware) => defaultMiddleware({ serializableCheck: false }),
});

export default store;
