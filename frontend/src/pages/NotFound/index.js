import React from 'react';
import { MainLayout } from '../../Layout';

const NotFound = () => <MainLayout>Страница не найдена</MainLayout>;

export default NotFound;
