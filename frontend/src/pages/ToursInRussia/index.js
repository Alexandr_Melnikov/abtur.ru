import React from 'react';

import { MainLayout } from '../../Layout';

const toursInRussia = () => (
  <MainLayout>
    <h1>Туры по России</h1>
  </MainLayout>
);

export default toursInRussia;
