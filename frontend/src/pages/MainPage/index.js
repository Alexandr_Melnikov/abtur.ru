import React from 'react';

import { Grid, Box } from '@mui/material';

import Carousel from '../../Components/Carousel';
// import Caption from '../../Components/Caption';
import AdvantagesCard from '../../Components/AdvantagesCard';
import SpecialOffersCard from '../../Components/SpecialOffersCard';
import OurTeam from '../../Components/OurTeam';
import Reviews from '../../Components/Reviews';
import News from '../../Components/News';

import { dataCard, specialOffers } from '../../utils/data';

// import styles from './mainPage.scss';
import { MainLayout } from '../../Layout';

const mainPage = () => (
  <MainLayout>
    <Carousel />
    <Grid container justifyContent="space-around">
      {dataCard.map((card) => (
        <Box my={5} key={card.id}>
          <AdvantagesCard title={card.title} desc={card.desc} img={card.img} />
        </Box>
      ))}
    </Grid>
    <Grid container justifyContent="space-between">
      {specialOffers.map((offers) => (
        <Box pb={5} key={offers.id}>
          <SpecialOffersCard name={offers.name} img={offers.img} url={offers.url} />
        </Box>
      ))}
    </Grid>
    <Grid container justifyContent="space-between" spacing={6}>
      <Grid item xs={12} md={6}>
        <OurTeam />
      </Grid>
      <Grid item xs={12} md={6}>
        <Reviews />
      </Grid>
    </Grid>
    <Grid container justifyContent="space-between" spacing={6}>
      <Grid item xs={12} md={6}>
        <News />
      </Grid>
    </Grid>
  </MainLayout>
);

export default mainPage;
