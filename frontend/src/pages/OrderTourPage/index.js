import React from 'react';

import { Grid } from '@mui/material';
import { MainLayout } from '../../Layout';

// import styles from './OrderTourPage.scss';

import SmallFormTourOrder from '../../Components/SmallFormTourOrder';
import DetailFormTourOrder from '../../Components/DetailFormTourOrder';

const OrderTourPage = () => (
  <MainLayout>
    <Grid container justifyContent="space-between" spacing={6}>
      <Grid item xs={12} md={6}>
        <SmallFormTourOrder />
      </Grid>
      <Grid item xs={12} md={6}>
        <DetailFormTourOrder />
      </Grid>
    </Grid>
  </MainLayout>
);

export default OrderTourPage;
