import React from 'react';

import { MainLayout } from '../../Layout';

const aboutCompanyPage = () => (
  <MainLayout>
    <h1>О компании</h1>
  </MainLayout>
);

export default aboutCompanyPage;
