import express from 'express';
import config from 'config';
import mongoose from 'mongoose';
import cors from 'cors';

import authRouter from './routes/api/auth.js';
import contactsRouter from './routes/api/contacts.js';
import adminPanelRouter from './routes/api/adminPanel.js';
import commonRouter from './routes/api/common.js';
import feedbackRouter from './routes/api/feedBack.js';

const app = express({});
const PORT = config.get('PORT');

async function startServer() {
  try {
    await mongoose.connect(config.get('mongoURI'),
      {
        useNewUrlParser: true,
        useUnifiedTopology: true,
      });
    app.listen(PORT, () => console.log('MongoDB Connected') || console.log(`Server started on port ${PORT}`));
  } catch (error) {
    console.log('Server error:', error.message);
    process.exit(1);
  }
}

startServer();

// Init Middleware
app.use(express.json());
app.use(cors());

app.get('/', (req, res) => res.send('API Running'));

// Define Routes
app.use('/api/auth', authRouter);
app.use('/api/contacts', contactsRouter);
app.use('/api/admin-panel', adminPanelRouter);
app.use('/api/common', commonRouter);
app.use('/api/feedback', feedbackRouter);
